import React, { Component, lazy, Suspense } from 'react';
import { Bar, Doughnut, Line, Pie, Polar, HorizontalBar } from 'react-chartjs-2';
import PropTypes from 'prop-types';

import {
  Badge,
  Button,
  ButtonDropdown,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Table, CardColumns
} from 'reactstrap';
import {
  ComposableMap,
  Geographies,
  Geography,
  Marker
} from "react-simple-maps";
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';


const axios = require('axios');


    export default function Dashboard() {


      const [isReady, setIsReady] = React.useState(false);


      const [clothesListFavorites, setClothesListFavorites] = React.useState({name: null, numberOfFavorites: null});
      const [clothesGenderList, setClothesListGender] = React.useState(null);


      //Is Color Blind
      const [isColorBlind, setIsColorBlind] = React.useState(0);
      const [isNotColorBlind, setIsNotColorBlind] = React.useState(0);

      //Type of color blind
      const [isDeuteranopia, setIsDeuteranopia] = React.useState(0);
      const [isTritanopia, setIsTritanopia] = React.useState(0);
      const [isMonochromacy, setIsMonochromacy] = React.useState(0);
      const [isNone, setIsNone] = React.useState(0);

      //Trend type
      const [isBusiness, setIsBusiness] = React.useState(0);
      const [isCasual, setIsCasual] = React.useState(0);
      const [isSemiCasual, setIsSemiCasual] = React.useState(0);
      const [isAlternative, setIsAlternative] = React.useState(0);
      const [isAll, setIsAll] = React.useState(0);

      //Most engaged users
      const [userListFavorites, setUserListFavorites] = React.useState({users: null, numberOfFavorites: null});
      const [finishRegistration, setFinishRegistration] = React.useState({registered: null, notRegistered: null});

      const [isEngage, setIsEngage] = React.useState(0);
      const [notEngage, setNotEngage] = React.useState(0);


      const [tagsInfo, setTagsInfo] = React.useState({tagLabel: null, tagRep: null});


      //Stores locations
      const [mapStores, setMapStores] = React.useState([{
        markerOffset: 0,
        name: null,
        coordinates: [0,0]
      }]);


      React.useEffect(() => {

        // Make a request for a user with a given ID
        axios.get('https://firestore.googleapis.com/v1/projects/huestyle-f28b8/databases/(default)/documents/stores?pageSize=300')
          .then(function (response) {
            // handle success
            let storesArr = [];

            response.data.documents.forEach(function(obj) {
              storesArr.push({
                  markerOffset: -30,
                  name: obj.fields.name.stringValue,
                  coordinates: [obj.fields.long.doubleValue, obj.fields.lat.doubleValue]
                })
            });
            setMapStores(storesArr);

          })
          .catch(function (error) {
            // handle error
            console.log(error);
          })


        // Make a request for a user with a given ID
        axios.get('https://firestore.googleapis.com/v1/projects/huestyle-f28b8/databases/(default)/documents/clothing?pageSize=300')
          .then(function (response) {
            // handle success
            console.log(response.data.documents);

            let usersFavorited = [];
            let userNameSorted = [];
            let userNumberLikesSorted = [];

            let male = 0;
            let female = 0;

            let tagsArray = [];
            let labelTags = [];
            let likeTags = [];

            response.data.documents.forEach(function(obj) {
              usersFavorited.push([obj.fields.name.stringValue, obj.fields.liked.integerValue])
              usersFavorited.sort()
              if (obj.fields.gender.stringValue === "Mens") {
                male++;
              } else {
                female++;
              }
              let tags = obj.fields.tags.stringValue.split(',');
              for (let i = 0; i < tags.length; i++) {
                if (tagsArray.find(element => element.value === tags[i])){
                  for (let j = 0; j < tagsArray.length; j++) {
                    if (tagsArray[j].value===tags[i]) {
                      tagsArray[j].repeats++;
                      break
                    }
                  }
                } else {
                  tagsArray.push({value: tags[i], repeats: 1})
                }
              }
            });
            for (let i = 0; i < tagsArray.length; i++) {
              labelTags.push(tagsArray[i].value);
              likeTags.push(tagsArray[i].repeats);
            }
            setTagsInfo({tagLabel: labelTags, tagRep: likeTags});

            for (var i = 0; i < usersFavorited.length; i++) {
              userNameSorted.push(usersFavorited[i][0])
              if (usersFavorited[i][1]) {
                userNumberLikesSorted.push(usersFavorited[i][1])
              } else {
                userNumberLikesSorted.push(0)
              }
            }

            setClothesListFavorites({name:userNameSorted, numberOfFavorites: userNumberLikesSorted})
            setClothesListGender([male,female]);
          })
          .catch(function (error) {
            // handle error
            console.log(error);
          })



          // Make a request for a user with a given ID
          axios.get('https://firestore.googleapis.com/v1/projects/huestyle-f28b8/databases/(default)/documents/users?pageSize=300')
            .then(function (response) {
              // handle success
              let isBlind = 0;
              let isNotBlind = 0;

              let eng = 0;
              let notEng = 0;

              let tritanopia = 0;
              let deutetranopia = 0;
              let monochromacy = 0;
              let none = 0;

              let business = 0;
              let casual = 0;
              let semicasual = 0;
              let alternative = 0;
              let all = 0;

              let usersFavorited = [];
              let userNameSorted = [];
              let userNumberLikesSorted = [];

              let registered = 0;
              let nonRegistered = 0;

              console.log(response.data.documents);
              response.data.documents.forEach(function(obj) {

                if (obj.fields.onBoardingFinished) {
                  if (obj.fields.onBoardingFinished.booleanValue) {
                    registered++;
                  } else {
                    nonRegistered++;
                  }
                } else {
                  nonRegistered++;
                }

                if (obj.fields.colorBlind) {
                  if (obj.fields.colorBlind.booleanValue) {
                    isBlind++;
                    if (obj.fields.colorBlindType){
                      if (obj.fields.colorBlindType.stringValue === "Tritanopia") {
                        tritanopia++;
                      }
                      else if (obj.fields.colorBlindType.stringValue === "Deutenaropia") {
                        deutetranopia++;
                      } else {
                        monochromacy++;
                      }
                    }
                    else {
                      monochromacy++;
                    }
                  } else {
                    none++;
                    isNotBlind++;
                  }
                } else {
                  none++;
                  isNotBlind++;
                }
                if (obj.fields.styleType.stringValue === "All") {
                  all++;
                } else {
                  if (obj.fields.styleType.stringValue === "Business") {
                    business++;
                  } else if (obj.fields.styleType.stringValue === "Casual") {
                    casual++;
                  } else if (obj.fields.styleType.stringValue === "Semi-Casual") {
                    semicasual++;
                  } else {
                    alternative++;
                  }
                }
                if (obj.fields.favorites.arrayValue.values) {
                  usersFavorited.push([obj.fields.name.stringValue, obj.fields.favorites.arrayValue.values.length])
                } else {
                  usersFavorited.push([obj.fields.name.stringValue, 0])
                }
                //usersFavorited.sort()

              });

              for (let i = 0; i < usersFavorited.length; i++) {
                userNameSorted.push(usersFavorited[i][0])
                if (usersFavorited[i][1]) {
                  eng++;
                  userNumberLikesSorted.push(usersFavorited[i][1])
                } else {
                  notEng++;
                  userNumberLikesSorted.push(0)
                }
              }
              setIsEngage(eng)
              setNotEngage(notEng)

              setIsColorBlind(isBlind)
              setIsNotColorBlind(isNotBlind)

              setIsTritanopia(tritanopia)
              setIsDeuteranopia(deutetranopia)
              setIsMonochromacy(monochromacy)
              setIsNone(none)

              setIsBusiness(business)
              setIsCasual(casual)
              setIsSemiCasual(semicasual)
              setIsAlternative(alternative)
              setIsAll(all)

              setUserListFavorites({users:userNameSorted, numberOfFavorites: userNumberLikesSorted})

              setFinishRegistration({registered: registered, notRegistered: nonRegistered})

              setIsReady(true);

            })
            .catch(function (error) {
              // handle error
              console.log(error);
            })
      },[]);





      const hasColorBlind = {
        labels: [
          'Is colorblind',
          'Is not colorblind'
        ],
        datasets: [
          {
            data: [isColorBlind, isNotColorBlind],
            backgroundColor: [
              '#FF6384',
              '#36A2EB',
              '#FFCE56',
            ],
            hoverBackgroundColor: [
              '#FF6384',
              '#36A2EB',
              '#FFCE56',
            ],
          }],
      };

      const gender = {
        labels: [
          'Male',
          'Female'
        ],
        datasets: [
          {
            data: clothesGenderList,
            backgroundColor: [
              '#36A2EB',
              '#FF6384',
            ],
            hoverBackgroundColor: [
              '#36A2EB',
              '#FF6384',
            ],
          }],
      };

      const pie = {
        labels: [
          'Deuteranopia',
          'Tritanopia',
          'Monochromacy',
        ],
        datasets: [
          {
            data: [isDeuteranopia, isTritanopia, isMonochromacy],
            backgroundColor: [
              '#FF6384',
              '#36A2EB',
              '#FFCE56',
            ],
            hoverBackgroundColor: [
              '#FF6384',
              '#36A2EB',
              '#FFCE56',
            ],
          }],
      };

      const pieEng = {
        labels: [
          'Engaged',
          'Not Engaged',
        ],
        datasets: [
          {
            data: [isEngage, notEngage],
            backgroundColor: [
              '#FF6384',
              '#36A2EB',
              '#FFCE56',
            ],
            hoverBackgroundColor: [
              '#FF6384',
              '#36A2EB',
              '#FFCE56',
            ],
          }],
      };

      const doughnut = {
        labels: [
          'Business',
          'Casual',
          'Semi-Casual',
          'Alternative',
          'All'
        ],
        datasets: [
          {
            data: [isBusiness, isCasual, isSemiCasual, isAlternative, isAll],
            backgroundColor: [
              '#FF6384',
              '#36A2EB',
              '#FFCE56',
              '#333333',
            ],
            hoverBackgroundColor: [
              '#FF6384',
              '#36A2EB',
              '#FFCE56',
              '#333333',
            ],
          }],
      };

      const bar = {
        labels: userListFavorites.users,
        datasets: [
          {
            label: 'User Likes',
            backgroundColor: 'rgba(255,99,132,0.2)',
            borderColor: 'rgba(255,99,132,1)',
            borderWidth: 1,
            hoverBackgroundColor: 'rgba(255,99,132,0.4)',
            hoverBorderColor: 'rgba(255,99,132,1)',
            data: userListFavorites.numberOfFavorites,
          },
        ],
      };

      const barClothes = {
        labels: clothesListFavorites.name,
        datasets: [
          {
            label: 'Clothes',
            backgroundColor: 'rgba(255,99,132,0.2)',
            borderColor: 'rgba(255,99,132,1)',
            borderWidth: 1,
            hoverBackgroundColor: 'rgba(255,99,132,0.4)',
            hoverBorderColor: 'rgba(255,99,132,1)',
            data: clothesListFavorites.numberOfFavorites,
          },
        ],
      };

      const onboarding = {
        labels: ["User Onboarding"],
        datasets: [
          {
            label: 'Registered',
            backgroundColor: '#FF6384',
            borderColor: '#FF6384',
            data: [finishRegistration.registered],
          },
          {
            label: 'Not Registered',
            backgroundColor: "#36A2EB",
            borderColor: "#36A2EB",
            data: [finishRegistration.notRegistered],
          }
        ],
      };

      const options = {
        tooltips: {
          enabled: false,
          custom: CustomTooltips
        },
      }

      const data = {
        labels: tagsInfo.tagLabel,
        datasets: [
          {
            label: 'My First dataset',
            backgroundColor: 'rgba(255,99,132,0.2)',
            borderColor: 'rgba(255,99,132,1)',
            borderWidth: 1,
            hoverBackgroundColor: 'rgba(255,99,132,0.4)',
            hoverBorderColor: 'rgba(255,99,132,1)',
            data: tagsInfo.tagRep
          }
        ]
      };
      //loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>

      return (
        <div className="animated fadeIn">
        <CardColumns className="cols-2">

            <Card>
              <CardHeader>
                Colorblind Type
              </CardHeader>
              <CardBody>
                <div className="chart-wrapper">
                  <Pie data={pie} />
                </div>
              </CardBody>
            </Card>

            <Card>
              <CardHeader>
                Users with color blindness
              </CardHeader>
              <CardBody>
                <div className="chart-wrapper">
                  <Pie data={hasColorBlind} />
                </div>
              </CardBody>
            </Card>

            <Card>
              <CardHeader>
                Trend Type By User
              </CardHeader>
              <CardBody>
                <div className="chart-wrapper">
                  <Doughnut data={doughnut} />
                </div>
              </CardBody>
            </Card>

            <Card>
              <CardHeader>
                Gender
              </CardHeader>
              <CardBody>
                <div className="chart-wrapper">
                  <Pie data={gender} />
                </div>
              </CardBody>
            </Card>

            <Card>
              <CardHeader>
                Users that finished account registration
              </CardHeader>
              <CardBody>
                <div className="chart-wrapper">
                  <Bar data={onboarding} options={options} />
                </div>
              </CardBody>
            </Card>

          <Card>
            <CardHeader>
              Engage users by likes activity
            </CardHeader>
            <CardBody>
              <div className="chart-wrapper">
                <Pie data={pieEng} />
              </div>
            </CardBody>
          </Card>

          <Card>
            <CardHeader>
              Map Stores
            </CardHeader>
            <CardBody>
              <div className="chart-wrapper">
                <MapChart stores={mapStores}/>
              </div>
            </CardBody>
          </Card>

          </CardColumns>

          <Card>
            <CardHeader>
              Trendy Tags
            </CardHeader>
            <CardBody>
              <div className="chart-wrapper">
                <HorizontalBar data={data} />
              </div>
            </CardBody>
          </Card>

          <Card>
            <CardHeader>
              Users with more favorited clothes
            </CardHeader>
            <CardBody>
              <div className="chart-wrapper">
                <Bar data={bar} options={options} />
              </div>
            </CardBody>
          </Card>

          <Card>
            <CardHeader>
              Clothes with more likes
            </CardHeader>
            <CardBody>
              <div className="chart-wrapper">
                <Bar data={barClothes} options={options} />
              </div>
            </CardBody>
          </Card>

        </div>
      )
    }


    const geoUrl = "https://raw.githubusercontent.com/zcreativelabs/react-simple-maps/master/topojson-maps/world-110m.json";


const MapChart = (stores) => {

      return (
        <ComposableMap
            projection="geoAzimuthalEqualArea"
            projectionConfig={{
              rotate: [58.0, 20.0],
              scale: 400
            }}>
          <Geographies geography={geoUrl}>
            {({ geographies }) =>
              geographies
                  .filter(d => d.properties.REGION_UN === "Americas")
                  .map(geo => (
                  <Geography
                    key={geo.rsmKey}
                    geography={geo}
                    fill="#EAEAEC"
                    stroke="#D6D6DA"
                  />
                ))
            }
          </Geographies>
          {stores.stores.map(({ name, coordinates, markerOffset }) => (
            <Marker key={name} coordinates={coordinates}>
              <g
                fill="none"
                stroke="#FF5533"
                strokeWidth="2"
                strokeLinecap="round"
                strokeLinejoin="round"
                transform="translate(-12, -24)"
              >
                <circle cx="12" cy="10" r="3" />
                <path d="M12 21.7C17.3 17 20 13 20 10a8 8 0 1 0-16 0c0 3 2.7 6.9 8 11.7z" />
              </g>
              <text
                textAnchor="middle"
                y={markerOffset}
                style={{ fontFamily: "system-ui", fill: "#5D5A6D" }}
              >
                {name}
              </text>
            </Marker>
          ))}
        </ComposableMap>
      );
    };

//export default MapChart;
